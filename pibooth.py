#!/usr/bin/python3

import picamera
from picamera import PiCamera
#from picamera import Color
from time import sleep

camera = PiCamera()
#colour = Color

camera.preview_alpha = 255 #preview opacity, 0-255

camera.resolution = (640, 480)
camera.framerate = 24
camera.start_preview()
#camera.annotate_background = picamera.Color('yellow')

camera.annotate_text = "Taking photo in..."
sleep(2)
camera.annotate_text = "...3..."
sleep(1)
camera.annotate_text = "...2..."
sleep(1)
camera.annotate_text = "...1..."
sleep(1)
camera.annotate_text = "Hello world!"
#for i in range(4):
sleep(5)
camera.capture('/home/pi/Desktop/test.jpg')
camera.stop_preview()

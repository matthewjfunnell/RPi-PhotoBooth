#

# When importing the RPi-Photobooth python code for the first time, 
# the raspberry pi camera module needs to be switched on. Execute: 
	sudo raspi-config
# And scroll through:
	Interfacing Options -> P1 camera
